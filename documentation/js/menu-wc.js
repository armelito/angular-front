'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">front-angular documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AdminModule.html" data-type="entity-link" >AdminModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AdminModule-8d880644bfbcd3b64d11d4290dcf5a0f180301a6a831741411d1ab5777cefb71857aecdec818e00e5af01fcb6f377e874fe235966839105a4dcf8518a5e6ff76"' : 'data-target="#xs-components-links-module-AdminModule-8d880644bfbcd3b64d11d4290dcf5a0f180301a6a831741411d1ab5777cefb71857aecdec818e00e5af01fcb6f377e874fe235966839105a4dcf8518a5e6ff76"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AdminModule-8d880644bfbcd3b64d11d4290dcf5a0f180301a6a831741411d1ab5777cefb71857aecdec818e00e5af01fcb6f377e874fe235966839105a4dcf8518a5e6ff76"' :
                                            'id="xs-components-links-module-AdminModule-8d880644bfbcd3b64d11d4290dcf5a0f180301a6a831741411d1ab5777cefb71857aecdec818e00e5af01fcb6f377e874fe235966839105a4dcf8518a5e6ff76"' }>
                                            <li class="link">
                                                <a href="components/AheaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AheaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AlayoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AlayoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DashboardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DashboardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SidemenuComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SidemenuComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AdminRoutingModule.html" data-type="entity-link" >AdminRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-e0030e1357e7b0efe350f033099f2fe355539bc043ee31b8f87a3660a786b3e56856556d39aa5537219bc20ad3146db57494db96b75b484c88357c658aade414"' : 'data-target="#xs-components-links-module-AppModule-e0030e1357e7b0efe350f033099f2fe355539bc043ee31b8f87a3660a786b3e56856556d39aa5537219bc20ad3146db57494db96b75b484c88357c658aade414"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-e0030e1357e7b0efe350f033099f2fe355539bc043ee31b8f87a3660a786b3e56856556d39aa5537219bc20ad3146db57494db96b75b484c88357c658aade414"' :
                                            'id="xs-components-links-module-AppModule-e0030e1357e7b0efe350f033099f2fe355539bc043ee31b8f87a3660a786b3e56856556d39aa5537219bc20ad3146db57494db96b75b484c88357c658aade414"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ErrorComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ErrorComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link" >AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AuthModule-a17c03687dcc5982b0b5fc692f14f9d9ad0dd4d8cc34032a3fe4f338f5d17819b8f57dd2a383dbf0f6c2abb73d3eded85ef7dccb9500e873a4506b29e969e992"' : 'data-target="#xs-components-links-module-AuthModule-a17c03687dcc5982b0b5fc692f14f9d9ad0dd4d8cc34032a3fe4f338f5d17819b8f57dd2a383dbf0f6c2abb73d3eded85ef7dccb9500e873a4506b29e969e992"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AuthModule-a17c03687dcc5982b0b5fc692f14f9d9ad0dd4d8cc34032a3fe4f338f5d17819b8f57dd2a383dbf0f6c2abb73d3eded85ef7dccb9500e873a4506b29e969e992"' :
                                            'id="xs-components-links-module-AuthModule-a17c03687dcc5982b0b5fc692f14f9d9ad0dd4d8cc34032a3fe4f338f5d17819b8f57dd2a383dbf0f6c2abb73d3eded85ef7dccb9500e873a4506b29e969e992"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LogoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LogoutComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthRoutingModule.html" data-type="entity-link" >AuthRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CorrectionModule.html" data-type="entity-link" >CorrectionModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CorrectionModule-65e1f526ffb69c989c9a9f1c99190f1d3b838ffec0faac81ad52b270cbfa99b1a04bf04f064e16466365778ffe929a08b44eae9ed34dfe8255a1c9342409ddc8"' : 'data-target="#xs-components-links-module-CorrectionModule-65e1f526ffb69c989c9a9f1c99190f1d3b838ffec0faac81ad52b270cbfa99b1a04bf04f064e16466365778ffe929a08b44eae9ed34dfe8255a1c9342409ddc8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CorrectionModule-65e1f526ffb69c989c9a9f1c99190f1d3b838ffec0faac81ad52b270cbfa99b1a04bf04f064e16466365778ffe929a08b44eae9ed34dfe8255a1c9342409ddc8"' :
                                            'id="xs-components-links-module-CorrectionModule-65e1f526ffb69c989c9a9f1c99190f1d3b838ffec0faac81ad52b270cbfa99b1a04bf04f064e16466365778ffe929a08b44eae9ed34dfe8255a1c9342409ddc8"' }>
                                            <li class="link">
                                                <a href="components/CAddComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CAddComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CDeleteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CDeleteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CEditComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CIndexComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CIndexComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CorrectionRoutingModule.html" data-type="entity-link" >CorrectionRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/MaintenanceModule.html" data-type="entity-link" >MaintenanceModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MaintenanceModule-c30dd1cd9dfaa67958dbde2a409b93c7ed1db6d7590708535180147f6816b085ced82c05e1b3e81f4742d66b0d7a352a8ab010ca77285e0d937e9906614b4644"' : 'data-target="#xs-components-links-module-MaintenanceModule-c30dd1cd9dfaa67958dbde2a409b93c7ed1db6d7590708535180147f6816b085ced82c05e1b3e81f4742d66b0d7a352a8ab010ca77285e0d937e9906614b4644"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MaintenanceModule-c30dd1cd9dfaa67958dbde2a409b93c7ed1db6d7590708535180147f6816b085ced82c05e1b3e81f4742d66b0d7a352a8ab010ca77285e0d937e9906614b4644"' :
                                            'id="xs-components-links-module-MaintenanceModule-c30dd1cd9dfaa67958dbde2a409b93c7ed1db6d7590708535180147f6816b085ced82c05e1b3e81f4742d66b0d7a352a8ab010ca77285e0d937e9906614b4644"' }>
                                            <li class="link">
                                                <a href="components/MAddComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MAddComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MDeleteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MDeleteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MEditComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MIndexComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MIndexComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MaintenanceRoutingModule.html" data-type="entity-link" >MaintenanceRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/OperationModule.html" data-type="entity-link" >OperationModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/OperationRoutingModule.html" data-type="entity-link" >OperationRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PublicModule.html" data-type="entity-link" >PublicModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PublicModule-962e86f0d6e0421a3c908d66e3fcac952810d1accbce01255752027bcbeab058fc2ce8e2d3a14cefd5bab9f461eb12ae7f76e2e31d41c95cc13e89c6a359603e"' : 'data-target="#xs-components-links-module-PublicModule-962e86f0d6e0421a3c908d66e3fcac952810d1accbce01255752027bcbeab058fc2ce8e2d3a14cefd5bab9f461eb12ae7f76e2e31d41c95cc13e89c6a359603e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PublicModule-962e86f0d6e0421a3c908d66e3fcac952810d1accbce01255752027bcbeab058fc2ce8e2d3a14cefd5bab9f461eb12ae7f76e2e31d41c95cc13e89c6a359603e"' :
                                            'id="xs-components-links-module-PublicModule-962e86f0d6e0421a3c908d66e3fcac952810d1accbce01255752027bcbeab058fc2ce8e2d3a14cefd5bab9f461eb12ae7f76e2e31d41c95cc13e89c6a359603e"' }>
                                            <li class="link">
                                                <a href="components/HomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PheaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PheaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PlayoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PlayoutComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PublicRoutingModule.html" data-type="entity-link" >PublicRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserModule-ad2a7929ffea620f1997fd86ff702e89f9ac65d68f80743e9ecc1e13f438c4d71759db47b1746e1604da8df78e020441b3b0c321b2093c9209b18b8a5e0998d6"' : 'data-target="#xs-components-links-module-UserModule-ad2a7929ffea620f1997fd86ff702e89f9ac65d68f80743e9ecc1e13f438c4d71759db47b1746e1604da8df78e020441b3b0c321b2093c9209b18b8a5e0998d6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserModule-ad2a7929ffea620f1997fd86ff702e89f9ac65d68f80743e9ecc1e13f438c4d71759db47b1746e1604da8df78e020441b3b0c321b2093c9209b18b8a5e0998d6"' :
                                            'id="xs-components-links-module-UserModule-ad2a7929ffea620f1997fd86ff702e89f9ac65d68f80743e9ecc1e13f438c4d71759db47b1746e1604da8df78e020441b3b0c321b2093c9209b18b8a5e0998d6"' }>
                                            <li class="link">
                                                <a href="components/UAddComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UAddComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UDeleteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UDeleteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UEditComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UIndexComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UIndexComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserRoutingModule.html" data-type="entity-link" >UserRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UserTypeModule.html" data-type="entity-link" >UserTypeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserTypeModule-15bb18ec4ea211d88b95beca9e82c32bb4481cba88924258f45b1aa7de60e8d9d2ba1f85caee251c7ce07865f8cf4dcc4928890aacde7f0831ecb1dbc16a47cd"' : 'data-target="#xs-components-links-module-UserTypeModule-15bb18ec4ea211d88b95beca9e82c32bb4481cba88924258f45b1aa7de60e8d9d2ba1f85caee251c7ce07865f8cf4dcc4928890aacde7f0831ecb1dbc16a47cd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserTypeModule-15bb18ec4ea211d88b95beca9e82c32bb4481cba88924258f45b1aa7de60e8d9d2ba1f85caee251c7ce07865f8cf4dcc4928890aacde7f0831ecb1dbc16a47cd"' :
                                            'id="xs-components-links-module-UserTypeModule-15bb18ec4ea211d88b95beca9e82c32bb4481cba88924258f45b1aa7de60e8d9d2ba1f85caee251c7ce07865f8cf4dcc4928890aacde7f0831ecb1dbc16a47cd"' }>
                                            <li class="link">
                                                <a href="components/UtAddComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UtAddComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UtDeleteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UtDeleteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UtEditComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UtEditComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UtIndexComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UtIndexComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserTypeRoutingModule.html" data-type="entity-link" >UserTypeRoutingModule</a>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TokenService.html" data-type="entity-link" >TokenService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link" >AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ICredential.html" data-type="entity-link" >ICredential</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IToken.html" data-type="entity-link" >IToken</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});