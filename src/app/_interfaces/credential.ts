// Les interfaces ne sont utilisées par Angular qu'à la compilation. Elles permettent uniquement de vérifier que les données attendues reçues suivent une structure précise (type)
// Par convention, on met tjs un I devant le nom de l'interface
// L'interface agit comme un contrat qui va définir le type des variables // //// acceptées

export interface ICredential {
  pseudo: string,
  password: string
}
