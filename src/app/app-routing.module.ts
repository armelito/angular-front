import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './_helpers/auth.guard';
import { ErrorComponent } from './_utils/error/error.component';

// routage principal de l'app - c'est ici que l'on branche les module
const routes: Routes = [

  // import des modules public et admin  puis chargement de ces derniers
  {
    path: '', loadChildren: () => import('./public/public.module')
      .then(m => m.PublicModule),

  },
  {
    // ATTENTION: ici branchement du gardien pour fermer les routes de l'admin
    path:'admin', loadChildren: () => import('./admin/admin.module')
      .then(m => m.AdminModule), canActivate:[AuthGuard]
  },
  {
    path:'auth', loadChildren: () => import('./auth/auth.module')
      .then(m => m.AuthModule)
  },

  // le ErrorComponent s'affichera si après avoir checké toutes les autres urls, la page demandée n'existe pas.
  { path:'**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
