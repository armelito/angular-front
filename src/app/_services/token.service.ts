import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private router: Router) { }
// stockage du token dans le navigateur
  saveToken(token: string): void {
    localStorage.setItem('token', token)
    // url de routage si login ok
    this.router.navigate(['admin'])
  }

  // boolean qui renvoie true si token ok et false si null
  isLogged(): boolean {
    const token = localStorage.getItem('token')
    console.log(token)
    return !! token
  }

  //suppression du token à la déconnexion
  clearToken(): void {
    localStorage.removeItem('token')
    this.router.navigate(['/'])
  }

  //suppression du token expiré
    clearTokenExpired(): void {
      localStorage.removeItem('token')
      this.router.navigate(['auth'])
    }

  // récupération du token après l'interceptor
  getToken(): string | null {
    return localStorage.getItem('token')
  }
}
