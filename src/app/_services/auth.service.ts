import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICredential } from '../_interfaces/credential';
import { IToken } from '../_interfaces/token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  url = 'http://localhost:8888/auth/login'

  constructor(private http: HttpClient) { }

  // on indique à HttpClient qu'il va recevoir un objet de type token avec post<IToken>
  //Signature de la méthode = nom de la méthode + parmètres + return (ici de type Observable)
  // credentials (par convention car la méthode reçoit login + password)
  login(credentials: ICredential): Observable<IToken> {
    return this.http.post<IToken>(this.url, credentials)
  }

}
