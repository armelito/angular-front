import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { TokenService } from '../_services/token.service';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenService) {}


  // la méthode intercept reçoit la request et le gestionnaire de request
  // ensuite après avoir éffectué l'action requise le gestionnaire renvoie la request qu'il a reçu
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    console.log(request)
    const token = this.tokenService.getToken()

    // Si token à insérer dans le header
    // alors on clone la request originale pour y insérer le header avec le token ( on ne modifie pas la request originale)
    if (token !== null) {
      let clone = request.clone({
        headers:request.headers.set('Authorization', 'bearer '+token)
      })
      console.log(clone)
      return next.handle(clone).pipe(
        catchError(error => {
          console.log(error)

          if (error.status === 401) {
          this.tokenService.clearTokenExpired()
          }
          return throwError ('Session expired')
        })
      )
    }
    return next.handle(request);
  }
}

export const TokenInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: TokenInterceptor,
  multi: true
}
