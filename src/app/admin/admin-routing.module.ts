import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlayoutComponent } from './alayout/alayout.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path:'', component: AlayoutComponent, children: [
      { path:'', redirectTo: 'dashboard', pathMatch: 'full' },
      { path:'dashboard', component: DashboardComponent },
      { // import du module user puis chargement de ce dernier
        path:'user', loadChildren: () => import('./user/user.module')
          .then(m => m.UserModule)
      },
      {
        path:'user-type', loadChildren: () => import('./user-type/user-type.module')
          .then(m => m.UserTypeModule)

      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
