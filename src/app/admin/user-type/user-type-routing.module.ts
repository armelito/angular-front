import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UtAddComponent } from './ut-add/ut-add.component';
import { UtDeleteComponent } from './ut-delete/ut-delete.component';
import { UtEditComponent } from './ut-edit/ut-edit.component';
import { UtIndexComponent } from './ut-index/ut-index.component';

// Routage des composants (index, edit, add, delete) du module User
// /:id = définition du pattern
const routes: Routes = [
  { path: '', component: UtIndexComponent },
  { path: 'edit/:id', component: UtEditComponent },
  { path: 'add', component: UtAddComponent },
  { path: 'delete/:id', component: UtDeleteComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserTypeRoutingModule { }
