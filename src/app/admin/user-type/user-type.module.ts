import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserTypeRoutingModule } from './user-type-routing.module';
import { UtIndexComponent } from './ut-index/ut-index.component';
import { UtEditComponent } from './ut-edit/ut-edit.component';
import { UtAddComponent } from './ut-add/ut-add.component';
import { UtDeleteComponent } from './ut-delete/ut-delete.component';


@NgModule({
  declarations: [
    UtIndexComponent,
    UtEditComponent,
    UtAddComponent,
    UtDeleteComponent
  ],
  imports: [
    CommonModule,
    UserTypeRoutingModule
  ]
})
export class UserTypeModule { }
