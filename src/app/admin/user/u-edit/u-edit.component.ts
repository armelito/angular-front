import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-u-edit',
  templateUrl: './u-edit.component.html',
  styleUrls: ['./u-edit.component.css']
})
export class UEditComponent implements OnInit {
// injection de la dépendance ActivatedRoute dans le constucteur
// les services sont des observables (promesse en javascript)
// gère l'état d'un opération (en cours, succes, échec)
// en général, on nomme la variable avec le nom du service injecté mais sans majuscule
  constructor(private activated: ActivatedRoute) { }
// initialisation de la page
  ngOnInit(): void {
    // on souscrit à la promesse (on se branche sur le canal par lequel vont passer les données)
    this.activated.params.subscribe(
      (data) => {
        console.log(data)
      }
    )
  }

}
