import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_services/user.service'

@Component({
  selector: 'app-u-index',
  templateUrl: './u-index.component.html',
  styleUrls: ['./u-index.component.css']
})
export class UIndexComponent implements OnInit {
nom = '';
users: any;


  constructor(
  private http: HttpClient,
  private userService: UserService,
  ) { }

  ngOnInit(): void {

    this.userService.getUser().subscribe(
      data => console.log(data),
      //data => this.users= data,
      err => console.error('ici error', err)
    )
//     this.http.get('http://localhost:8888/users').subscribe(
//       data => console.log(data),
//     )
  }


}
