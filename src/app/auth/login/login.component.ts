import { Component, OnInit } from '@angular/core';
import { ICredential } from 'src/app/_interfaces/credential';
import { AuthService } from 'src/app/_services/auth.service';
import { TokenService } from 'src/app/_services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
// déclaration et initialisation de l'objet form et de ses propriétés
  form: ICredential = {
    pseudo: '',
    password: '',
  }

  // injection des service - un service permet d'utiliser le même code dans tous les composants de l'application - facilite l'échange de données et réduit les risques d'erreur
  constructor(
    private authService: AuthService,
    private tokenService: TokenService
    ) { }

  // la ngOninit se déclenche au moment où l'on affiche la page
  ngOnInit(): void {
  }
  onSubmit(): void {
    console.log(this.form)
    // souscription à l'observable et écriture des promesses
    this.authService.login(this.form).subscribe(
      data => {
        // sauvegarde du token (avec methode saveToken du service token)
        console.log(data.access_token)
        this.tokenService.saveToken(data.access_token)
      },
      err => console.log(err)
    )
  }

}
