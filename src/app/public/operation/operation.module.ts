import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaintenanceModule } from './maintenance/maintenance.module';
import { CorrectionModule } from './correction/correction.module';
import { OperationRoutingModule } from './operation-routing.module';



@NgModule({
  declarations: [

  ],
  imports: [
    CommonModule,
    MaintenanceModule,
    CorrectionModule,
    OperationRoutingModule,
  ]
})
export class OperationModule { }
