import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CorrectionRoutingModule } from './correction-routing.module';
import { CAddComponent } from './c-add/c-add.component';
import { CEditComponent } from './c-edit/c-edit.component';
import { CIndexComponent } from './c-index/c-index.component';
import { CDeleteComponent } from './c-delete/c-delete.component';


@NgModule({
  declarations: [
    CAddComponent,
    CEditComponent,
    CIndexComponent,
    CDeleteComponent
  ],
  imports: [
    CommonModule,
    CorrectionRoutingModule
  ],

  exports: [
    CAddComponent,
    CEditComponent,
    CIndexComponent,
    CDeleteComponent
  ]
})
export class CorrectionModule { }
