import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MAddComponent } from './m-add/m-add.component';
import { MIndexComponent } from './m-index/m-index.component';
import { MEditComponent } from './m-edit/m-edit.component';
import { MDeleteComponent } from './m-delete/m-delete.component';
import { MaintenanceRoutingModule } from './maintenance-routing.module';


@NgModule({
  declarations: [
    MAddComponent,
    MIndexComponent,
    MEditComponent,
    MDeleteComponent
  ],
  imports: [
    CommonModule,
    MaintenanceRoutingModule
  ],
  exports: [
    MAddComponent,
    MIndexComponent,
    MEditComponent,
    MDeleteComponent,
  ]
})
export class MaintenanceModule { }
