import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OperationModule } from './operation.module';
import { MaintenanceModule } from './maintenance/maintenance.module';
import { MAddComponent } from './maintenance/m-add/m-add.component';


const routes: Routes = [
  {
    path: 'maintenance',
    loadChildren:() => import('./maintenance/maintenance.module').then(m => m.MaintenanceModule)
  },
  {
    path: 'correction',
    loadChildren:() => import('./correction/correction.module').then(m => m.CorrectionModule)
  },

]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperationRoutingModule { }
