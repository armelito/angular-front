import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MaintenanceModule } from './operation/maintenance/maintenance.module';
import { PlayoutComponent } from './playout/playout.component';

const routes: Routes = [

  {
    path: '', component: PlayoutComponent, children: [
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: 'home', component: HomeComponent },
      {
        path: 'operation', loadChildren: () => import('./operation/operation.module')
          .then(m => m.OperationModule),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
